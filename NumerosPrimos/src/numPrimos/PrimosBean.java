package numPrimos;

import java.util.Random;

public class PrimosBean{
	
	int numero;
	int numRand;// El numero de primos a generar
	int numIntentos = 0;
	boolean acierto = true; // A partir de que n�mero se generan los primos
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int num) {
		this.numero = num;
	}
	
	public int getIntentos() {
		return numIntentos;
	}
	
	public int getNumRand() {
		return numRand;
	}
	
	
	public boolean esMayor(int num) {
		numIntentos++;
		return (numRand > numero);
	}
	
	public void calcularNumAleatorio(){
		if (acierto) {
			numRand = (int)(Math.random()*100) + 1;
			acierto = false;
		}
	}
	
	public void haAcertado() {
		acierto = true;
	}
}