package ejemploServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletError extends HttpServlet {
	
	private String nombre;
	private String apellidos;

	public void init(ServletConfig config) throws ServletException{
		// Llamada al m�todo init() de la superclase (HttpServlet)
		// As� se asegura una correcta inicializaci�n del servlet
		super.init(config);
		System.out.println("Iniciando ServletOpinion...");
	}

	// Este m�todo es llamado por el servidor web al "apagarse" (shutdown)
	// Sirve para proporcionar una correcta desconexi�n de una base de 
	// datos, cerrar ficheros,...
	public void destroy(){
		System.out.println("No hay nada que hacer...");
	}
	
	// M�todo llamado autom�ticamente cuando se realiza una invocaci�n
	// al servlet a trav�s del m�todo HTTP GET.
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
throws ServletException, IOException{
		
		// Adquisici�n de los valores del formulario a trav�s del objeto req
		nombre = req.getParameter("nombre");
		apellidos = req.getParameter("apellidos");
		// Devolver al usuario una p�gina HTML con los valores adquiridos

		// En primer lugar se establece el tipo de contenido MIME de la respuesta
		resp.setContentType("text/html");

		// Se obtiene un PrintWriter donde escribir (s�lo para mandar texto)
		PrintWriter out = resp.getWriter();
		
		// Se genera el contenido de la p�gina HTML
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Valores recogidos en el formulario</title>");
		out.println("</head>");
		out.println("<body>");
		HttpSession session = req.getSession();
		nombre = (String) session.getAttribute("nombre");
		apellidos = (String) session.getAttribute("apellidos");
		if (!nombre.equalsIgnoreCase("iso3") && !apellidos.equalsIgnoreCase("iso3")) {
			out.println("<p><font-size=+1>El usuario/password introducido " + nombre + "/" + apellidos + " es incorrecto.<p></font>");
		}
		out.println("</body>");
		out.println("</html>");

		// Se fuerza la descarga del buffer y se cierra el PrintWriter.
		// De esta forma se libera el recurso.
		out.flush();
		out.close();
	}

	// Funci�n que permite al servidor web obtener una peque�a descripci�n del
	// servlet, que cometido tiene, nombre del autor, etc.
	public String getServletInfo(){
		return 	"Este servlet lee los datos de un formulario" + 
					" y los muestra en pantalla";
	}
	
}
