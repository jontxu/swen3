package ejemploServlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ServletOpinion extends HttpServlet{

	// Declaraci�n de variables miembro correspondientes a los campos 
	// de formulario
	private String nombre;
	private String apellidos;
	private String opinion;
	private String comentarios;

	// Este m�todo se ejecuta una �nica vez (al ser inicializado el servlet)
	// Se suelen inicializar variables y realizar operaciones costosas en
	// tiempo de ejecuci�n (conexiones a BD,...)
	public void init(ServletConfig config) throws ServletException{
		// Llamada al m�todo init() de la superclase (HttpServlet)
		// As� se asegura una correcta inicializaci�n del servlet
		super.init(config);
		System.out.println("Iniciando ServletOpinion...");
	}

	// Este m�todo es llamado por el servidor web al "apagarse" (shutdown)
	// Sirve para proporcionar una correcta desconexi�n de una base de 
	// datos, cerrar ficheros,...
	public void destroy(){
		System.out.println("No hay nada que hacer...");
	}
	
	// M�todo llamado autom�ticamente cuando se realiza una invocaci�n
	// al servlet a trav�s del m�todo HTTP GET.
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
throws ServletException, IOException{
		
		// Adquisici�n de los valores del formulario a trav�s del objeto req
		nombre = req.getParameter("nombre");
		apellidos = req.getParameter("apellidos");
		// Devolver al usuario una p�gina HTML con los valores adquiridos

		// En primer lugar se establece el tipo de contenido MIME de la respuesta
		resp.setContentType("text/html");

		// Se obtiene un PrintWriter donde escribir (s�lo para mandar texto)
		PrintWriter out = resp.getWriter();
		
		// Se genera el contenido de la p�gina HTML
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Valores recogidos en el formulario</title>");
		out.println("</head>");
		out.println("<body>");
		if (nombre.equalsIgnoreCase("iso3") && apellidos.equalsIgnoreCase("iso3")) {
			out.println("<p><font-size=+1>Bienvenido!</font><p>");
		}
		else {
			out.println("<p>Error: Validacion incorrecta! <a href='ServletErrorURL'>Ver detalles del error.</a></p>");
			HttpSession session = req.getSession(true);
			session.setAttribute("nombre", new String(nombre));
			session.setAttribute("apellidos", new String(apellidos));
		}
		out.println("</body>");
		out.println("</html>");

		// Se fuerza la descarga del buffer y se cierra el PrintWriter.
		// De esta forma se libera el recurso.
		out.flush();
		out.close();
	}

	// Funci�n que permite al servidor web obtener una peque�a descripci�n del
	// servlet, que cometido tiene, nombre del autor, etc.
	public String getServletInfo(){
		return 	"Este servlet lee los datos de un formulario" + 
					" y los muestra en pantalla";
	}
}