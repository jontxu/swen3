package iso3.pt.modal;

public class Evaluacion {
	private int id;
	private String conc;
	private float grade;
	private Alumno a;
	private Asignatura as;
	
	
	public Evaluacion(int i, String c, float g, Alumno a, Asignatura as) {
		setId(i);
		setConc(c);
		setGrade(g);
		this.setA(a);
		this.setAs(as);
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getConc() {
		return conc;
	}


	public void setConc(String conc) {
		this.conc = conc;
	}


	public Alumno getA() {
		return a;
	}


	public void setA(Alumno a) {
		this.a = a;
	}


	public float getGrade() {
		return grade;
	}


	public void setGrade(float grade) {
		this.grade = grade;
	}


	public Asignatura getAs() {
		return as;
	}


	public void setAs(Asignatura as) {
		this.as = as;
	}
}
