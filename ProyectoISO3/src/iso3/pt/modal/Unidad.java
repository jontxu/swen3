package iso3.pt.modal;

public class Unidad {
 private int id;
 private String ac;
 private String tit;
 private String cont;
 
 Unidad(){
	 
 }
 
 Unidad(int i, String a, String t, String c){
	 setId(i);
	 setAc(a);
	 setTit(t);
	 setCont(c);
 }

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getAc() {
	return ac;
}

public void setAc(String ac) {
	this.ac = ac;
}

public String getTit() {
	return tit;
}

public void setTit(String tit) {
	this.tit = tit;
}

public String getCont() {
	return cont;
}

public void setCont(String cont) {
	this.cont = cont;
}
 
 
}
