package iso3.pt.modal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Asignatura {
	private int id;
	private int cod;
	private String nom;
	private float cred;
	private Set<Unidad> units;
	private Set<Alumno> alum;
	private Profesor p;
	
	public Asignatura(int i, int c, String n, float cr, Profesor p) {
		setId(i);
		setCod(c);
		setNom(n);
		setCred(cr);
		units = new HashSet<Unidad>();
		alum = new HashSet<Alumno>();
		this.setP(p);
	}
	
	public void adddAlum(Alumno a) {
		alum.add(a);
	}
	
	public void addUnit(Unidad u) {
		units.add(u);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getCred() {
		return cred;
	}

	public void setCred(float cred) {
		this.cred = cred;
	}
	
	public Profesor getP() {
		return p;
	}

	public void setP(Profesor p) {
		this.p = p;
	}
	
	boolean estaMatriculado(Alumno a) {
		return alum.contains(a);
	}
}
