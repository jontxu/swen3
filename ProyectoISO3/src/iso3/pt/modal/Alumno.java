package iso3.pt.modal;

import java.util.HashSet;
import java.util.Set;

public class Alumno {
	private int dni;
	private String pass;
	private String name;
	private String tlf;
	private Set<Asignatura> asig;
	
	public Alumno(int d, String p, String n, String t){
		setDni(d);
		setPass(p);
		setName(n);
		setTlf(t);
		asig = new HashSet<Asignatura>();	
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTlf() {
		return tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	
	public void addAsig(Asignatura a) {
		asig.add(a);
	}
}
