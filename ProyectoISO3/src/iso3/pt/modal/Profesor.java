package iso3.pt.modal;

import java.util.ArrayList;
import java.util.List;

public class Profesor {
	private int id;
	private int dni;
	private String pas;
	private String nombre;
	private String telf;
	private String email;
	private String desp;
	
	Profesor(int i, int d, String p, String n, String t, String e, String de) {
		setId(i);
		setDni(d);
		setPas(p);
		setNombre(n);
		setTelf(t);
		setEmail(e);
		setDesp(de);
	}

	Profesor() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getPas() {
		return pas;
	}

	public void setPas(String pas) {
		this.pas = pas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelf() {
		return telf;
	}

	public void setTelf(String telf) {
		this.telf = telf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}
	
}
