package iso3.pt.dao;

public class PtDAO {
	
	public static PtDAO instance = null;
	
	private PtDAO(){
		
	}
	
	public static PtDAO getInstance(){
		if (instance == null)
				instance = new PtDAO();
		return instance;
	}
}
