<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%! 
LanguageDesignerAction action = (LanguageDesignerAction)ActionContext.getContext().getActionInvocation().getAction(); 
%>


<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.lecturersubjects.title"/> <% action.getLecturerName(); %> (<% action.getLecturerID(); %>)</h1>
		<br/>
		<table>
			<tr>
				<td>
					<s:url id="urlLogout" action="showLogin" escapeAmp="false"/>
					<a href="<s:property value="#urlLogout"/>"><s:text name="label.lecturersubjects.logout"/></a>
				</td>
			</tr>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.lecturersubjects.code"/></th>
		        <th><s:text name="label.lecturersubjects.name"/></th>
		        <th><s:text name="label.lecturersubjects.credits"/></th>
		        <th><s:text name="label.lecturersubjects.lecturer"/></th>
		        <th><s:text name="label.lecturersubjects.units"/></th>
		        <th><s:text name="label.lecturersubjects.students"/></th>
		        <th><s:text name="label.lecturersubjects.list"/></th>
		        <th>&nbsp;&nbsp;</th>
		    </tr>
		    <s:iterator value="languageDesigners" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="code"/></td>
		            <td class="nowrap"><s:property value="name"/></td>
		            <td class="nowrap"><s:property value="credits"/></td>
		            <td class="nowrap"><s:property value="lecturer"/></td>
		            <td class="nowrap"><s:property value="units"/></td>
		            <td class="nowrap"><s:property value="students"/></td>
		            <td class="nowrap"><a href="#"><s:property value="students"/></a></td>
		        </tr>
		    </s:iterator>
		</table>
		
		<s:actionerror />
		<s:actionmessage />
		<form action="/struts2tutorial/iso3/languageDesigners!selectDesigner.action" method="POST">
			<table>
			<% List<> %> 
			<tr><td>
					<select name="selectedLanguageDesigner.fullName">
					<% 
						List<LanguageDesigner> designers = action.getLanguageDesigners();
						for (LanguageDesigner designer: designers) { %>
							<option value="<%= designer.getFullName() %>"><%= designer.getFullName() %></option> <%
						}
					%>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Entregar"/></td>
				<td colspan="2"><input type="submit" name="redirect-action:languageDesigners" value="Cancelar"/></td>
			</tr>	
		</form>
	</body>
</html>