package iso3.pt.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Element;

public class Asignatura {
	

	

	private Integer id;
	private int codigo;
	private String nombre;
	private float creditos;
	private Set<Alumno> alumnos;
	private Set<Profesor> profesores;
	private Set<Unidad> unidades;
	
	public Asignatura(){}
	public Asignatura(Integer id, int codigo, String nombre, float creditos)
	{
		super();
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
		this.creditos = creditos;
		this.alumnos = new HashSet<Alumno>();
		this.setProfesores(new HashSet<Profesor>());
		this.setUnidades(new HashSet<Unidad>());
		
	}
	
	public boolean estaMatriculado(Alumno alumno)
	{
		return  alumnos.contains(alumno);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getCreditos() {
		return creditos;
	}
	public void setCreditos(float creditos) {
		this.creditos = creditos;
	}
	public Set<Profesor> getProfesores() {
		return profesores;
	}
	public void setProfesores(Set<Profesor> profesores) {
		this.profesores = profesores;
	}
	
	public void addProfesor(Profesor profesor) {
		profesores.add(profesor);
	}
	
	public void removeProfesor(Profesor profesor) {
		profesores.remove(profesor);
	}
	
	public Set<Unidad> getUnidades() {
		return unidades;
	}
	public void setUnidades(Set<Unidad> unidades) {
		this.unidades = unidades;
	}
	
	public void addUnidad(Unidad unidad) {
		unidades.add(unidad);
	}
	
	public void removeUnidad(Unidad unidad) {
		unidades.remove(unidad);
	}
}
