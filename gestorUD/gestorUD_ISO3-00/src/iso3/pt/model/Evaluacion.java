package iso3.pt.model;

import iso3.pt.model.Alumno;
import iso3.pt.model.Asignatura;

public class Evaluacion {
	
	private Integer id;
	private String concepto;
	private float nota;
	private Alumno alumno;
	private Asignatura asignatura;
	
	public Evaluacion(){}
	
	
	
	public Evaluacion(Integer id, String concepto, float nota) {
		super();
		this.id = id;
		this.concepto = concepto;
		this.nota = nota;
	}



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public float getNota() {
		return nota;
	}
	public void setNota(float nota) {
		this.nota = nota;
	}

	public Alumno getAlumno() {
		return alumno;
	}



	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}



	public Asignatura getAsignatura() {
		return asignatura;
	}



	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

}
