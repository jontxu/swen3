package iso3.pt.dao;

import java.util.List;
import java.util.Set;

import iso3.pt.model.Alumno;
import iso3.pt.model.Asignatura;
import iso3.pt.model.Evaluacion;
import iso3.pt.model.Profesor;
import iso3.pt.model.Unidad;

public class PtDAO {
	
	public static PtDAO instance = null;
	
	private PtDAO() {
		
	}
	
	public static PtDAO getInstance() {
		if (instance == null)
				instance = new PtDAO();
		return instance;
	}
	
	public Profesor getProfesor(int idAsignatura) {
		return null;
		
	}
	
	public Set<Alumno> getAlumnos(int idAsignatura) {
		return null;
		
	}
	
	public List<Evaluacion> getEvaluacionesOrderedByAsignatura(int idAlumno) {
		return null;
		
	}
	
	public Set<Evaluacion> getEvaluaciones(int idAsignatura, int idAlumno) {
		return null;
	
	}
	
	public void addEvaluacion(String concepto, float nota, int idAsignatura, int idAlumno) {
		
	}
	
	public Set<Unidad> getUnidades(int idAsignatura) {
		return null;
		
	}
	
	public Set<Asignatura> getAsignaturas() {
		return null;
		
	}
	
	public Alumno getAlumno(int id) {
		return null;
		
	}
	
	public Asignatura getAsignatura(int id) {
		return null;
		
	}
	
	public Alumno loginAlumno(int dni, String pass) throws UserNotFoundException, IncorrectPasswordException {
		return null;
		
	}
	
	public Set<Asignatura> getAsignaturas(int idAlumno) {
		return null;
		
	}
	
	public void matricular(int idAlumno, int idAsignatura) {
		
	}
	
	public void desmatricular(int idAlumno, int idAsignatura) {
		
	}
	
	public Profesor loginProfesor(int dni, String pass) throws UserNotFoundException, IncorrectPasswordException {
		return null;
		
	}
	
	public Set<Asignatura> getAsignaturasProfesor(int idProfesor) {
		return null;
		
	}
	
	public Profesor getProfesorByDni(int dni) throws UserNotFoundException {
		return null;
		
	}
	
	public List<Evaluacion> getEvaluacionesAsignatura(int idAsignatura) {
		return null;
		
	}
}
